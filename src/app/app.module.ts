import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './modules/material/material.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {fakeBackendProvider} from './services/_authenticationServices/_helpers/fake-backend';
import {ErrorInterceptor} from './services/_authenticationServices/_helpers/error.interceptor';
import {JwtInterceptor} from './services/_authenticationServices/_helpers/jwt.interceptor';
import {appRoutingModule} from './modules/router/app.routing';
import { UserElementComponent } from './components/pagesElements/user-element/user-element.component';
import { UserLoginControllerComponent } from './components/modalWindowsControllers/user-login-controller/user-login-controller.component';
import { UserLoginWindowComponent } from './components/modalWindows/user-login-window/user-login-window.component';
import {ReactiveFormsModule} from '@angular/forms';
import { ProductEditorWindowComponent } from './components/modalWindows/product-editor-window/product-editor-window.component';
import { ProductEditorControllerComponent } from './components/modalWindowsControllers/product-editor-controller/product-editor-controller.component';
import { ProductEditorElementComponent } from './components/pagesElements/product-editor-element/product-editor-element.component';
import { CategoryEditorElementComponent } from './components/pagesElements/category-editor-element/category-editor-element.component';
import { ProductDeleteWindowComponent } from './components/modalWindows/product-delete-window/product-delete-window.component';
import { ProductDeleteControllerComponent } from './components/modalWindowsControllers/product-delete-controller/product-delete-controller.component';
import { HeaderElementComponent } from './components/pagesElements/header-element/header-element.component';
import { FooterElementComponent } from './components/pagesElements/footer-element/footer-element.component';
import { ProductsListElementComponent } from './components/pagesElements/productsList/products-list-element/products-list-element.component';
import { ProductListItemElementComponent } from './components/pagesElements/productsList/product-list-item-element/product-list-item-element.component';
import { TopMenuElementComponent } from './components/pagesElements/top-menu-element/top-menu-element.component';
import { MainPageComponent } from './components/pages/main-page/main-page.component';
import { AboutPageComponent } from './components/pages/about-page/about-page.component';
import { AddToCartElementComponent } from './components/pagesElements/add-to-cart-element/add-to-cart-element.component';
import { ProductControllMenuElementComponent } from './components/pagesElements/product-controll-menu-element/product-controll-menu-element.component';
import { ProductPageComponent } from './components/pages/product-page/product-page.component';
import { CategoryListWindowComponent } from './components/modalWindows/category-list-window/category-list-window.component';
import { CategoryListConterollerComponent } from './components/modalWindowsControllers/category-list-conteroller/category-list-conteroller.component';


@NgModule({
  declarations: [
    AppComponent,
    UserElementComponent,
    UserLoginControllerComponent,
    UserLoginWindowComponent,
    ProductEditorWindowComponent,
    ProductEditorControllerComponent,
    ProductEditorElementComponent,
    CategoryEditorElementComponent,
    ProductDeleteWindowComponent,
    ProductDeleteControllerComponent,
    HeaderElementComponent,
    FooterElementComponent,
    ProductsListElementComponent,
    ProductListItemElementComponent,
    TopMenuElementComponent,
    MainPageComponent,
    AboutPageComponent,
    AddToCartElementComponent,
    ProductControllMenuElementComponent,
    ProductPageComponent,
    CategoryListWindowComponent,
    CategoryListConterollerComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    appRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    fakeBackendProvider,
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    UserLoginWindowComponent,
    ProductEditorWindowComponent,
    ProductDeleteWindowComponent,
    CategoryListWindowComponent
  ]
})
export class AppModule { }
