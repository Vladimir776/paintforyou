﻿import { Routes, RouterModule } from '@angular/router';
import {MainPageComponent} from '../../components/pages/main-page/main-page.component';
import {ProductPageComponent} from '../../components/pages/product-page/product-page.component';


const routes: Routes = [
  { path: '', component: MainPageComponent},
  {path: 'product/:productId', component: ProductPageComponent},
  {path: 'category/:category', component: MainPageComponent}
];
export const appRoutingModule = RouterModule.forRoot(routes);
