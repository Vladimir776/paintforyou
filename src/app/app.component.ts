import { Component } from '@angular/core';
import {CategoryService} from './services/category/category-service.service';
import {toArray} from 'rxjs/operators';
import {HttpClient, HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'PaintToYou';
  constructor( ) {
  }

}
