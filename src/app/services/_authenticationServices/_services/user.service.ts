﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '../../../interfaces/user';
import {config} from 'rxjs';
const apiUrl =  'http://localhost:4200';
@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${apiUrl}/users`);
    }

    register(user: User) {
        return this.http.post(`${apiUrl}/users/register`, user);
    }

    delete(id: number) {
        return this.http.delete(`${apiUrl}/users/${id}`);
    }
}
