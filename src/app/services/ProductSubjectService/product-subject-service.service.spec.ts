import { TestBed } from '@angular/core/testing';

import { ProductSubjectService } from './product-subject.service';

describe('ProductSubjectServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProductSubjectService = TestBed.get(ProductSubjectService);
    expect(service).toBeTruthy();
  });
});
