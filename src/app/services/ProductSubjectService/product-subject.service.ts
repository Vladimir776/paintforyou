import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Product} from '../../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class ProductSubjectService {
  readonly productSubjectAdd = new Subject<Product>();
  readonly productSubjectDelete = new Subject<Product>();
  readonly productSubjectEdit = new Subject<Product>();
  constructor() { }
  public emitAdd(product: Product) {
    this.productSubjectAdd.next(product);
  }
  public emitDelete(product: Product) {
    this.productSubjectDelete.next(product);
  }
  public emitEdit(product: Product) {
    this.productSubjectEdit.next(product);
  }

}
