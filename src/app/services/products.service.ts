import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {filter, find, flatMap, map, mergeMap, reduce, scan, toArray} from 'rxjs/operators';
import {BehaviorSubject, from, pipe, Subject} from 'rxjs';
import {Product} from '../interfaces/product';
import {CategoryService} from './category/category-service.service';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private readonly apiLink = `../../assets/products.json`;
  constructor(private http: HttpClient, private categoryServer: CategoryService) { }
  readonly SERVER_URL = 'http://89.40.14.148:3000/api/product';
  getProductsFromJSON() {
    return this.http.get(this.apiLink);
  }
  private castToFrontAll() {
    return map<any, Product>( (value, index) => this.castToFrontProduct(value[index]) );
  }
  private castToFrontProduct(value: any) {
    return {
      name: value.title,
      vendorCode: value.article,
      category: value.category,
      price: value.price,            // Цена
      productExist: value.exists,    // Товар есть на складе
      characteristics: value.characteristics, // Массив характеристик
      describe: value.description,
      discountPrice: value.discount,   // Цена по скидке
      titleImageLink: value.photo,
      id: value._id
    };
  }
  private castToBackProduct(value: Product) {
    console.log(value);
    return {
      title: value.name,
      article: value.vendorCode,
      category: value.category,
      price: value.price,            // Цена
      exists: value.productExist,    // Товар есть на складе
      characteristics: value.characteristics, // Массив характеристик
      description: value.describe,
      discount: value.discountPrice,   // Цена по скидке
      photo: value.titleImageLink,
      id: value.id
    };
  }


  getProductsFromServer() {
    return this.http.get(this.SERVER_URL + '/all')
      .pipe(
        mergeMap((value) => from(value as [])) ,
        map<any, Product>((value, index) => this.castToFrontProduct(value)),
        mergeMap(value => this.categoryServer.getFrontCategoryForProductById(value)) ,
        toArray()
      );
  }

  getProductByID(id: number | string) {
    return  this.http.get(this.SERVER_URL + '/' + id).pipe(
       map<any, Product>((value, index) => this.castToFrontProduct(value.product)),
       mergeMap(value => this.categoryServer.getFrontCategoryForProductById(value)) ,
     );
  }
  getProductsByCategory(category: string) {
    this.http.get(this.SERVER_URL + '?category=' + category).subscribe(value => {console.log('heeeere'); console.log(value); });
    return this.http.get(this.SERVER_URL + '?category=' + category).pipe(
      this.castToFrontAll(),
      mergeMap(value => this.categoryServer.getFrontCategoryForProductById(value)) ,
      toArray()
    );
  }
  getProducts() {
    return this.getProductsFromServer();
  }
  addProduct(product: Product) {
    const body = this.castToBackProduct(product);
    return this.http.post(this.SERVER_URL, body);
  }
  removeProduct(id: string) {
    return this.http.post(this.SERVER_URL + '/' + id, {});
  }
  editProduct(product: Product) {
    return this.categoryServer.getBackCategoryForProduct(product).pipe(
      mergeMap(value => {
        const body = this.castToBackProduct(value);
        return this.http.patch(this.SERVER_URL + '/' + product.id, body);
      })
    );
  }
}
