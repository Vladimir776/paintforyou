import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {Product} from '../../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class CategorySubjectService {
  readonly categorySubjectAdd = new Subject<any>();
  readonly categorySubjectDelete = new Subject<any>();
  readonly categorySubjectEdit = new Subject<any>();
  constructor() { }
  public emitAdd(product: any) {
    this.categorySubjectAdd.next(product);
  }
  public emitDelete(product: any) {
    this.categorySubjectDelete.next(product);
  }
  public emitEdit(product: any) {
    this.categorySubjectEdit.next(product);
  }

}
