import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { from } from 'rxjs';
import {Category} from '../../interfaces/categoty';
import {filter, map, mergeMap, toArray} from 'rxjs/operators';
import {Product} from '../../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  readonly SERVER_URL = 'http://89.40.14.148:3000/api/category';
  constructor(private httpClient: HttpClient) { }

  private getAllCategoriesFromJson() {
    return this.httpClient.get('assets/category.json');
  }
  private getAllCategoriesFromServer() {
    return this.httpClient.get(this.SERVER_URL + '/all')
      .pipe(
        mergeMap<any, any>(value => from(value),
        )).pipe(
          map<any , Category>(value => ({id: value._id, categoryType: value.title})),
          toArray()
      );
  }

  public getAllCategories() {
    return this.getAllCategoriesFromServer();
  }

  public addCategory(title: string) {
    return this.httpClient.post(this.SERVER_URL , {title});
  }
  getFrontCategoryForProductById(product: Product) {
    return this.httpClient.get(this.SERVER_URL + '/all').pipe(
      mergeMap((value) => from(value as [])) ,
      filter<any>(value => value._id === product.category),
      map(value => {
        product.category = value.title;
        return product;
      })
    );
  }
  getBackCategoryForProduct(product: Product) {
    return this.httpClient.get(this.SERVER_URL + '/all').pipe(
      mergeMap((value) => from(value as [])) ,
      filter<any>(value => value.title === product.category),
      map(value => {
        product.category = value._id;
        return product;
      })
    );
  }
  public removeCategory(title: string) {
    return this.httpClient.post(this.SERVER_URL + '/' + title , {});
  }

  public editCategory(oldTitle: any , title: string) {

    return this.httpClient.get(this.SERVER_URL + '/' + oldTitle.categoryType)
      .pipe(
        map<any, any>(value => {
          value.title = title;
          return value;
        }),
        mergeMap(value => {
          return this.httpClient.patch(this.SERVER_URL + '/' + oldTitle.categoryType, {title: value.title});
        })
      );
  }

}
