export interface Product {
  id: string;
  name: string;             // Имя товара
  vendorCode: string;       // Артикул
  category: string;         // Категория
  price: string;            // Цена
  productExist: boolean;    // Товар есть на складе
  characteristics: {key: string , value: string}[]; // Массив характеристик
  describe: string;
  discountPrice?: string;   // Цена по скидке
  titleImageLink?: string; // (в данном случае строка является путем к картинке )
}
export const getEmptyProduct = (): Product => {
  return {
    id: '',
    name: '',             // Имя товара
    vendorCode: '',       // Артикул
    category: '',         // Категория
    price: '' ,          // Цена
    productExist: false,    // Товар есть на складе
    characteristics: [], // Массив характеристик
    describe: '',
    discountPrice: '',   // Цена по скидке
    titleImageLink: '' // (в данном случае строка является путем к картинке )
  };
}
