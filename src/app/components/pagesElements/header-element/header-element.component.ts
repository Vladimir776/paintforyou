import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../services/_authenticationServices/_services/authentication.service';

@Component({
  selector: 'app-header-element',
  templateUrl: './header-element.component.html',
  styleUrls: ['./header-element.component.css']
})
export class HeaderElementComponent implements OnInit {

  constructor(
    public authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
  }

}
