import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../../services/_authenticationServices/_services/authentication.service';

@Component({
  selector: 'app-user-element',
  templateUrl: './user-element.component.html',
  styleUrls: ['./user-element.component.css']
})
export class UserElementComponent implements OnInit {
  status ;
  logInVisible = true;
  constructor(public authenticationService: AuthenticationService) {

    this.status = authenticationService.listenUser;
  }

  ngOnInit() {
  }

  logout() {
    this.authenticationService.logout();
    this.logInVisible = true;
  }

  toggleLoginVisible() {
    this.logInVisible = false;
  }
}
