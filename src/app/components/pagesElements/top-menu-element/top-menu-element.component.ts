import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../../../services/category/category-service.service';
import {AuthenticationService} from '../../../services/_authenticationServices/_services/authentication.service';

@Component({
  selector: 'app-top-menu-element',
  templateUrl: './top-menu-element.component.html',
  styleUrls: ['./top-menu-element.component.css']
})
export class TopMenuElementComponent implements OnInit {
  categories: any;
  constructor(private categoryService: CategoryService) {
    this.categories = categoryService.getAllCategories();
  }

  ngOnInit() {
  }

}
