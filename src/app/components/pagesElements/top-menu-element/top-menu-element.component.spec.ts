import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopMenuElementComponent } from './top-menu-element.component';

describe('TopMenuElementComponent', () => {
  let component: TopMenuElementComponent;
  let fixture: ComponentFixture<TopMenuElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopMenuElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopMenuElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
