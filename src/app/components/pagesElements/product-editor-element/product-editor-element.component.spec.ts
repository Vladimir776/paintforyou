import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductEditorElementComponent } from './product-editor-element.component';

describe('ProductEditorElementComponent', () => {
  let component: ProductEditorElementComponent;
  let fixture: ComponentFixture<ProductEditorElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductEditorElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductEditorElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
