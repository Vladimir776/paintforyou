import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductListItemElementComponent } from './product-list-item-element.component';

describe('ProductListItemElementComponent', () => {
  let component: ProductListItemElementComponent;
  let fixture: ComponentFixture<ProductListItemElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductListItemElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListItemElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
