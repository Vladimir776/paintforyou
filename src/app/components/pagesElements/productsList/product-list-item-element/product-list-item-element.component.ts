import {Component, Input, OnInit} from '@angular/core';
import {getEmptyProduct, Product} from '../../../../interfaces/product';
import {AuthenticationService} from '../../../../services/_authenticationServices/_services/authentication.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-product-list-item-element',
  templateUrl: './product-list-item-element.component.html',
  styleUrls: ['./product-list-item-element.component.css']
})
export class ProductListItemElementComponent implements OnInit {
  @Input() productId = '0';
  @Input() product: Product = getEmptyProduct() ;
  constructor(
    public authenticationService: AuthenticationService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }
  hasImageLink() {
    return !!this.product.titleImageLink;
  }
}
