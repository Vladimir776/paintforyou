import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-products-list-element',
  templateUrl: './products-list-element.component.html',
  styleUrls: ['./products-list-element.component.css']
})
export class ProductsListElementComponent implements OnInit {

  @Input() products;
  constructor() { }
  ngOnInit() {
    //this.products.subscribe(value => console.log(value));
  }

}
