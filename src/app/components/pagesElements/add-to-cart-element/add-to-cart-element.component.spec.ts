import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToCartElementComponent } from './add-to-cart-element.component';

describe('AddToCartElementComponent', () => {
  let component: AddToCartElementComponent;
  let fixture: ComponentFixture<AddToCartElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddToCartElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToCartElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
