import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-add-to-cart-element',
  templateUrl: './add-to-cart-element.component.html',
  styleUrls: ['./add-to-cart-element.component.css']
})
export class AddToCartElementComponent implements OnInit {
  @Input()productsCounter = 0;
  @Input()disabledCart = false;

  @Output() productCountChange = new EventEmitter<number>();
  @Output() cartButtonPress = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  reduceProductCounter() {
    if (this.productsCounter > 0) {  this.productsCounter --; }
    this.productCountChange.emit(this.productsCounter);
  }

  increaseProductCounter() {
    this.productsCounter++;
    this.productCountChange.emit(this.productsCounter);
  }

  cartButtonPressed($event) {
    this.cartButtonPress.emit($event);
  }

}
