import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Product} from '../../../interfaces/product';

@Component({
  selector: 'app-product-controll-menu-element',
  templateUrl: './product-controll-menu-element.component.html',
  styleUrls: ['./product-controll-menu-element.component.css']
})
export class ProductControllMenuElementComponent implements OnInit {
  @Input()productId: string;
  @Input()product: Product;
  @Output() deletedEvent = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
  }

  kek() {
    console.log('kruks');
  }
}
