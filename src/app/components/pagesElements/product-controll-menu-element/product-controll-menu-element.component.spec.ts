import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductControllMenuElementComponent } from './product-controll-menu-element.component';

describe('ProductControllMenuElementComponent', () => {
  let component: ProductControllMenuElementComponent;
  let fixture: ComponentFixture<ProductControllMenuElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductControllMenuElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductControllMenuElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
