import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryEditorElementComponent } from './category-editor-element.component';

describe('CategoryEditorElementComponent', () => {
  let component: CategoryEditorElementComponent;
  let fixture: ComponentFixture<CategoryEditorElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryEditorElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryEditorElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
