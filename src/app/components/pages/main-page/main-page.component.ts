import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductService} from '../../../services/products.service';
import {ProductSubjectService} from '../../../services/ProductSubjectService/product-subject.service';



@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {

  public products;
  constructor(private productService: ProductService, private route: ActivatedRoute, private productSubject: ProductSubjectService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const category = params.get('category');
      // tslint:disable-next-line:max-line-length
      category !== null ? this.products = this.productService.getProductsByCategory(category) : this.products = this.productService.getProducts();
      // tslint:disable-next-line:max-line-length
      this.productSubject.productSubjectDelete.subscribe(value => {
        // tslint:disable-next-line:max-line-length
         category !== null ? this.products = this.productService.getProductsByCategory(category) :  this.products =  this.productService.getProducts();
        });
      this.productSubject.productSubjectAdd.subscribe(value => {
        // tslint:disable-next-line:max-line-length
        category !== null ? this.products = this.productService.getProductsByCategory(category) :  this.products =  this.productService.getProducts();
      });
      this.productSubject.productSubjectEdit.subscribe(value => {
        // tslint:disable-next-line:max-line-length
        category !== null ? this.products = this.productService.getProductsByCategory(category) :  this.products =  this.productService.getProducts();
      });
    });
  }
}
