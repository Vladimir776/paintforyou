import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductService} from '../../../services/products.service';
import {map} from 'rxjs/operators';
import {AuthenticationService} from '../../../services/_authenticationServices/_services/authentication.service';
import {ProductSubjectService} from '../../../services/ProductSubjectService/product-subject.service';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css']
})
export class ProductPageComponent implements OnInit {
  public product;
  public productId;
  // tslint:disable-next-line:max-line-length
  constructor(private productSubjectServiceService: ProductSubjectService, private route: ActivatedRoute, private productService: ProductService , private  rr: Router , public  authenticationService: AuthenticationService) {
    route.paramMap.subscribe(params => {
      this.productId = params.get('productId');
   //   this.product.subscribe(value => console.log(value));
      this.product = productService.getProductByID(this.productId);
      // tslint:disable-next-line:max-line-length
      this.productSubjectServiceService.productSubjectEdit.subscribe(value =>  this.product = productService.getProductByID(this.productId));
    });
  }
  ngOnInit() {
  }
}
