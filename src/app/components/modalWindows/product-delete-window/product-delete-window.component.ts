import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {HttpClient} from '@angular/common/http';
import {ProductService} from '../../../services/products.service';

@Component({
  selector: 'app-product-delete-window',
  templateUrl: './product-delete-window.component.html',
  styleUrls: ['./product-delete-window.component.css']
})
export class ProductDeleteWindowComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ProductDeleteWindowComponent>,
    private productService: ProductService,
    @Inject(MAT_DIALOG_DATA) public data: {id: string, deleted:any},
  ) { }

  ngOnInit() {
  }

  deleteItem() {
    this.data.deleted.deleted = true;
    this.productService.removeProduct(this.data.id).subscribe(this.data.deleted);
  }

}
