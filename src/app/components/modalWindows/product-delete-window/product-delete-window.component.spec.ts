import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDeleteWindowComponent } from './product-delete-window.component';

describe('ProductDeleteWindowComponent', () => {
  let component: ProductDeleteWindowComponent;
  let fixture: ComponentFixture<ProductDeleteWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDeleteWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDeleteWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
