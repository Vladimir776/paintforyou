import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLoginWindowComponent } from './user-login-window.component';

describe('UserLoginWindowComponent', () => {
  let component: UserLoginWindowComponent;
  let fixture: ComponentFixture<UserLoginWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLoginWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoginWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
