import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {User} from '../../../interfaces/user';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import {AuthenticationService} from '../../../services/_authenticationServices/_services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-login-window',
  templateUrl: './user-login-window.component.html',
  styleUrls: ['./user-login-window.component.css']
})
export class UserLoginWindowComponent implements OnInit {
  public userLoginGroup: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  hide = true;
  constructor(
    public dialogRef: MatDialogRef<UserLoginWindowComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {},
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.userLoginGroup = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnUrl =  '/';
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.userLoginGroup.controls; }

  onSubmit() {
    this.submitted = true;



    // stop here if form is invalid
    if (this.userLoginGroup.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f.username.value, this.f.password.value)
      .pipe(first())
      .subscribe(
        data => {
          this.dialogRef.close();
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.loading = false;
          this.openSnackBar(error, 'error!');
        });
  }
}
