import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryListWindowComponent } from './category-list-window.component';

describe('CategoryListWindowComponent', () => {
  let component: CategoryListWindowComponent;
  let fixture: ComponentFixture<CategoryListWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryListWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryListWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
