import {Component, Inject, Input, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {Category} from '../../../interfaces/categoty';
import {CategoryService} from '../../../services/category/category-service.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Product} from '../../../interfaces/product';
import validate = WebAssembly.validate;
import {HttpClient} from '@angular/common/http';
import {CategorySubjectService} from '../../../services/CategorySubject/category-subject.service';

@Component({
  selector: 'app-category-list-window',
  templateUrl: './category-list-window.component.html',
  styleUrls: ['./category-list-window.component.css']
})
export class CategoryListWindowComponent implements OnInit {
  categoriesFromGroup: FormGroup;

  // tslint:disable-next-line:max-line-length
  constructor(private categorySubjectService: CategorySubjectService, private categoryService: CategoryService, private formBuilder: FormBuilder,  public dialogRef: MatDialogRef<CategoryListWindowComponent>,
              @Inject(MAT_DIALOG_DATA) public data: {categories: [Category]}) {
    this.categoriesFromGroup = this.formBuilder.group(
      {
        categories:  this.formBuilder.array([
        ])
      }
    );
    this.data.categories.push({id: '' , categoryType: ''});
    data.categories.forEach(value => this.addCategories(value.categoryType));

    dialogRef.beforeClosed().subscribe(value => {
      this.data.categories.pop();
    } );
  }
  initCategories(category = '') {
    return this.formBuilder.group({
      category: [category],
    });
  }

  addCategories(category = '') {
    const control = this.categoriesFromGroup.get('categories') as FormArray;
    control.push(this.initCategories(category));
  }


  getCategories(form) {
    return form.controls.categories.controls;
  }


  removeCategories(i) {
    const control = this.categoriesFromGroup.get('categories') as FormArray;
    this.categoryService.removeCategory(control.value[i].category).subscribe(value => {
      control.removeAt(i);
      this.categorySubjectService.categorySubjectDelete.next(null);
    });
  }
  editCategory(i) {
    const control = this.categoriesFromGroup.get('categories') as FormArray;
    this.categoryService.editCategory(this.data.categories[i] , control.value[i].category).subscribe(value => {
      this.categorySubjectService.categorySubjectEdit.next(null);
      console.log(value);
    });
  }
  addCategory(i) {
    const control = this.categoriesFromGroup.get('categories') as FormArray;
    this.categoryService.addCategory(control.value[i].category).subscribe(value => {
      this.categorySubjectService.categorySubjectAdd.next(null);
      this.addCategories();
      console.log(control.value);
    });
  }
  ngOnInit() {
  }



}
