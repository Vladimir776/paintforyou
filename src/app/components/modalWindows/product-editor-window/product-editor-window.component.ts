import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {getEmptyProduct, Product} from '../../../interfaces/product';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {CategoryService} from '../../../services/category/category-service.service';
import {ProductService} from '../../../services/products.service';
import {ProductSubjectService} from '../../../services/ProductSubjectService/product-subject.service';

@Component({
  selector: 'app-product-editor-window',
  templateUrl: './product-editor-window.component.html',
  styleUrls: ['./product-editor-window.component.css']
})
export class ProductEditorWindowComponent implements OnInit {
  public productGroup: FormGroup;
  readonly categories;
  readonly imageRegExp = new RegExp('^https?://\\S+(?:jpg|jpeg|png)$');
  // tslint:disable-next-line:max-line-length
   public tempProduct: Product =  {id: '', name: '', vendorCode: '', category: '', price: '0', productExist: false, characteristics: [], describe: ''};
  constructor(
    private productSubjectServiceService: ProductSubjectService,
    private categoryService: CategoryService,
    public dialogRef: MatDialogRef<ProductEditorWindowComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {create: boolean ,  product: Product},
    private fb: FormBuilder,
    private productService: ProductService
  ) {
    this.categories = categoryService.getAllCategories();
  }

  ngOnInit() {
    console.log( this.data.product );
    this.productClone(this.data.product , this.tempProduct);
    this.productGroup =
      this.fb.group({
        name: [this.tempProduct.name],
        titleImageLink: [this.tempProduct.titleImageLink],
        price: [this.tempProduct.price],
        vendorCode: [this.tempProduct.vendorCode],
        category: [this.tempProduct.category],
        describe: [this.tempProduct.describe],
        discountPrice: [this.tempProduct.discountPrice],
        productExist: [this.tempProduct.productExist],
        characteristics: this.fb.array([
          this.initCharacteristics(),
        ]),
      });
    const control = <FormArray> this.productGroup.get('characteristics');
    if (this.tempProduct.characteristics.length) { control.clear(); }
    this.tempProduct.characteristics.forEach(value => this.addCharacteristics(value.key, value.value));
    this.productGroup.valueChanges.subscribe(value => {
      this.tempProduct.name = value.name;
      this.tempProduct.titleImageLink = this.imageRegExp.test(value.titleImageLink) ? value.titleImageLink : '';
      this.tempProduct.price = value.price;
      this.tempProduct.discountPrice = value.discountPrice;
      this.tempProduct.vendorCode = value.vendorCode;
      this.tempProduct.category = value.category;
      this.tempProduct.productExist = value.productExist;
      this.tempProduct.characteristics = value.characteristics;
      this.tempProduct.describe = value.describe;
    });
  }

  initCharacteristics(key = '', value = '') {
    return this.fb.group({
      key: [key],
      value: [value],
    });
  }

  addCharacteristics(key= '', value = '') {
    const control = <FormArray> this.productGroup.get('characteristics');
    control.push(this.initCharacteristics(key, value));
  }


  getCharacteristics(form) {
    return form.controls.characteristics.controls;
  }


  removeCharacteristics(i) {
    const control = <FormArray> this.productGroup.get('characteristics');
    control.removeAt(i);
  }
  onSendClick() {
    this.productClone(this.tempProduct , this.data.product);
    // tslint:disable-next-line:max-line-length
    return this.data.create ?  this.productService.addProduct(this.tempProduct).subscribe(value => this.productSubjectServiceService.emitAdd(null)) :
      this.productService.editProduct(this.tempProduct).subscribe(value => this.productSubjectServiceService.emitEdit(null));
  }


  onSubmit(form) {

  }

  getPrompt() {
    return this.data.create ? 'Создать' : 'Изменить';
  }

  private productClone(productIn: Product , productOut: Product) {
    productOut.id = productIn.id;
    productOut.name = productIn.name;
    productOut.vendorCode = productIn.vendorCode;
    productOut.category = productIn.category;
    productOut.price = productIn.price;
    productOut.productExist = productIn.productExist;
    productOut.characteristics = productIn.characteristics;
    productOut.describe = productIn.describe;
    productOut.titleImageLink = productIn.titleImageLink ? productIn.titleImageLink : '';
    productOut.discountPrice = productIn.discountPrice ? productIn.discountPrice : '';
  }
}
