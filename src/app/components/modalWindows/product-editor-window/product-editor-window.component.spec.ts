import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductEditorWindowComponent } from './product-editor-window.component';

describe('ProductEditorWindowComponent', () => {
  let component: ProductEditorWindowComponent;
  let fixture: ComponentFixture<ProductEditorWindowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductEditorWindowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductEditorWindowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
