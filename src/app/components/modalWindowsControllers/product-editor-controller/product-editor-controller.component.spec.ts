import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductEditorControllerComponent } from './product-editor-controller.component';

describe('ProductEditorControllerComponent', () => {
  let component: ProductEditorControllerComponent;
  let fixture: ComponentFixture<ProductEditorControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductEditorControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductEditorControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
