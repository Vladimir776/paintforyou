import {Component, Input, OnInit} from '@angular/core';
import {getEmptyProduct, Product} from '../../../interfaces/product';
import {ProductEditorWindowComponent} from '../../modalWindows/product-editor-window/product-editor-window.component';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-product-editor-controller',
  templateUrl: './product-editor-controller.component.html',
  styleUrls: ['./product-editor-controller.component.css']
})
export class ProductEditorControllerComponent implements OnInit {
  @Input() product: Product = getEmptyProduct();
  @Input() matLabel: string;
  @Input() productName: string;
  @Input() productMethod = 'GET';
  @Input() create = true;
  constructor(public dialog: MatDialog) { }

  openDialog(): void {
    const dialogRef = this.dialog.open(ProductEditorWindowComponent, {
      width: '750px',
      height: '750px',
      data: {create: this.create , product: this.product}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (this.create) { this.product = getEmptyProduct(); }
    });
  }

  ngOnInit() {
  }

}
