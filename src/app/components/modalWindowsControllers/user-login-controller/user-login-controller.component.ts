import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {UserLoginWindowComponent} from '../../modalWindows/user-login-window/user-login-window.component';

@Component({
  selector: 'app-user-login-controller',
  templateUrl: './user-login-controller.component.html',
  styleUrls: ['./user-login-controller.component.css']
})
export class UserLoginControllerComponent implements OnInit {
  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(UserLoginWindowComponent, {
      width: '750px',
      height: '150px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
