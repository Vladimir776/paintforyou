import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLoginControllerComponent } from './user-login-controller.component';

describe('UserLoginControllerComponent', () => {
  let component: UserLoginControllerComponent;
  let fixture: ComponentFixture<UserLoginControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLoginControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoginControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
