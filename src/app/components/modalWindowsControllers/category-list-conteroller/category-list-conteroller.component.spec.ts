import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryListConterollerComponent } from './category-list-conteroller.component';

describe('CategoryListConterollerComponent', () => {
  let component: CategoryListConterollerComponent;
  let fixture: ComponentFixture<CategoryListConterollerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryListConterollerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryListConterollerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
