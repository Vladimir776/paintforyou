import {Component, Input, OnInit} from '@angular/core';
import {ProductEditorWindowComponent} from '../../modalWindows/product-editor-window/product-editor-window.component';
import {CategoryListWindowComponent} from '../../modalWindows/category-list-window/category-list-window.component';
import {MatDialog} from '@angular/material';
import {CategoryService} from '../../../services/category/category-service.service';
import {toArray} from 'rxjs/operators';
import {CategorySubjectService} from '../../../services/CategorySubject/category-subject.service';

@Component({
  selector: 'app-category-list-conteroller',
  templateUrl: './category-list-conteroller.component.html',
  styleUrls: ['./category-list-conteroller.component.css']
})
export class CategoryListConterollerComponent implements OnInit {
  private arrWrap = {arr: []};
  constructor(public dialog: MatDialog, private categoryService: CategoryService , categorySubjectService: CategorySubjectService) {
    categoryService.getAllCategories().pipe(toArray<any>()).subscribe(value => this.arrWrap.arr = value );
    categorySubjectService.categorySubjectAdd.subscribe(value =>
      categoryService.getAllCategories().pipe(toArray<any>()).subscribe(value1 => {
        return this.arrWrap.arr = value1;
      } )
    );
    categorySubjectService.categorySubjectDelete.subscribe(value =>
      categoryService.getAllCategories().pipe(toArray<any>()).subscribe(value1 => {
        return this.arrWrap.arr = value1;
      } )
    );
    categorySubjectService.categorySubjectEdit.subscribe(value =>
      categoryService.getAllCategories().pipe(toArray<any>()).subscribe(value1 => {
        return this.arrWrap.arr = value1;
      } )
    );
  }
  categories = [{id: '' , categoryType: ''}];
  openDialog(): void {
    const dialogRef = this.dialog.open(CategoryListWindowComponent, {
      width: '450px',
      height: '750px',
      data: {categories: this.arrWrap.arr[0]}
    });
    dialogRef.afterClosed().subscribe(result => {
     console.log( this.arrWrap.arr[0]);
    });
  }
  ngOnInit() {
  }

}
