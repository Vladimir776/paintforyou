import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDeleteControllerComponent } from './product-delete-controller.component';

describe('ProductDeleteControllerComponent', () => {
  let component: ProductDeleteControllerComponent;
  let fixture: ComponentFixture<ProductDeleteControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDeleteControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDeleteControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
