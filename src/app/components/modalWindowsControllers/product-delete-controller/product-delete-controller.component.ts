import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ProductDeleteWindowComponent} from '../../modalWindows/product-delete-window/product-delete-window.component';
import {ProductSubjectService} from '../../../services/ProductSubjectService/product-subject.service';

@Component({
  selector: 'app-product-delete-controller',
  templateUrl: './product-delete-controller.component.html',
  styleUrls: ['./product-delete-controller.component.css']
})
export class ProductDeleteControllerComponent implements OnInit {
  @Input() productId;
  @Output() deletedEvent = new EventEmitter<any>();
  private deleted = {deleted: false};
  constructor(public dialog: MatDialog, private productSubjectServiceService: ProductSubjectService) { }

  ngOnInit() {
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(ProductDeleteWindowComponent, {
      width: '250px',
      height: '150px',
      data: {id: this.productId , deleted: this.deleted}
    });
    dialogRef.afterClosed().subscribe(result => {
      if (this.deleted.deleted) { this.deletedEvent.emit(); this.productSubjectServiceService.emitDelete(null); }
    });
  }
}
